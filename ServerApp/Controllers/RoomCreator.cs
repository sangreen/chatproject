﻿using Query.Client;
using ServerApp.Infrastructure;
using ServerApp.Objects;
using System;

namespace ServerApp.Controllers
{
    public static class RoomCreator
    {
        #region Creater
        public static Room Create(string title, bool isPrivate)
        {
            var handler = Handler.CorrectRoomParameters(title);
            if (!handler.Item1)
                throw new Exception(handler.Item2);
            var id = ServerObject.Rooms.Count;
            var room = new ServerRoom(title, id, isPrivate);
            ServerObject.RegisterRoom(room);
            return new Room
            {
                ID = id,
                Data = title,
                Action = Query.Client.Action.Create,
            };
        }
        #endregion
    }
}
