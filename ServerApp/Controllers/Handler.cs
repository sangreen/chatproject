﻿using ServerApp.Objects;
using System;
using System.Linq;

namespace ServerApp.Controllers
{
    public static class Handler
    {
        #region Room Handler
        public static Tuple<bool, string> CorrectRoomParameters(string name)
        {
            if (!name.BelongsToSegment(Constant.MinLengthRoomTitle, Constant.MaxLengthRoomTitle))
                return new Tuple<bool, string>(false, $"Incorrect tittle length. "
                    + $"Expected {Constant.MinLengthRoomTitle}-{Constant.MaxLengthRoomTitle}");
            if (RoomExisting(name))
                return new Tuple<bool, string>(false, "The room was already created");
            return new Tuple<bool, string>(true, "Success");
        }

        private static bool RoomExisting(string name)
        {
            var roomCount = ServerObject.Rooms
                .Where(x => String.Compare(x.Title, name, StringComparison.OrdinalIgnoreCase) == 0)
                .Count();
            return roomCount != 0;
        }
        #endregion

        #region Nick Handler
        public static Tuple<bool, string> CorrectNickParameters(string nick)
        {
            if (!nick.BelongsToSegment(Constant.MinLengthNick, Constant.MaxLengthNick))
                return new Tuple<bool, string>(false, $"Incorrect nick length. Expected {Constant.MinLengthNick} - {Constant.MaxLengthNick}");
            if (PlayerExisting(nick))
                return new Tuple<bool, string>(false, $"A user with this name is already authorized");
            return new Tuple<bool, string>(true, "Success");
        }

        private static bool PlayerExisting(string nickname)
        {
            return ServerObject.Users
                .Select(x => x)
                .Where(x => String.Compare(x.UserName, nickname, StringComparison.OrdinalIgnoreCase) == 0)
                .Count() != 0;
        }
        #endregion
    }
}
