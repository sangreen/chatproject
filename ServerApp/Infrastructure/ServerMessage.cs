﻿using Query;
using ServerApp.Objects;
using System.Linq;
using System.Text;

namespace ServerApp.Infrastructure
{
    public class ServerMessage : Message
    {
        #region Public Members
        public ClientObject SenderClient { get; private set; }
        public ServerRoom Room { get; private set; }
        public byte[] Bytes { get; private set; }
        #endregion

        #region Constructor
        public ServerMessage(ClientObject client, string text, string sender, int roomID) : base(text, sender, roomID)
        {
            SenderClient = client;
            Room = ServerObject.Rooms
                .Select(x => x)
                .Where(x => x.ID == roomID)
                .FirstOrDefault();
            Bytes = Encoding.Unicode.GetBytes(text);
        }
        #endregion
    }
}