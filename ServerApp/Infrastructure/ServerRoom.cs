﻿using ServerApp.Objects;
using System.Collections.Generic;

namespace ServerApp.Infrastructure
{
    public class ServerRoom
    {
        #region Public Members
        public string Title { get; private set; }
        public int ID { get; private set; }
        public bool IsPrivate { get; private set; }
        public List<ClientObject> Users { get; private set; } = new List<ClientObject>();
        #endregion

        #region Constructor
        public ServerRoom(string name, int id, bool isPrivate)
        {
            Title = name;
            ID = id;
            IsPrivate = isPrivate;
        }
        #endregion

        #region Functions
        public void Remove(ClientObject client)
        {
            if (Users.Contains(client))
                Users.Remove(client);
            if (Users.Count < 1)
                ServerObject.Rooms.Remove(this);
        }

        public void Invite(ClientObject client)
        {
            if (Users.Count > 2 && IsPrivate)
            {
                ServerObject.SendFail(client, "Can not invite user in Private Chat :(");
                return;
            }

            if (!Users.Contains(client) && Users.Count <= Constant.MaxPeopleCountInRoom)
            {
                Users.Add(client);
                client.RoomList.Add(this);
            }
        }
        #endregion
    }
}
