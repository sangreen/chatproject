﻿namespace ServerApp
{
    public static class StringExtensions
    {
        public static bool BelongsToSegment(this string arg, int min, int max)
        {
            return (min <= arg.Length && arg.Length <= max);
        }
    }
}
