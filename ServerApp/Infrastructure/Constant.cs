﻿namespace ServerApp
{
    public static class Constant
    {
        public const int MinLengthRoomTitle = 4;
        public const int MaxLengthRoomTitle = 20;
        public const int MinLengthNick = 5;
        public const int MaxLengthNick = 20;
        public const int MaxPeopleCountInRoom = 30;
        public const int MaxMessageLength = 150;
        public const string RoomClassName = "Room";
        public const string MessageClassName = "Message";
        public const string InviteClassName = "InviteToRoom";
    }
}
