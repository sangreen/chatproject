﻿using ServerApp.Objects;
using System;
using System.Threading;

namespace ServerApp
{
    public class Program
    {
        private static Thread listenThread;
        static void Main(string[] args)
        {
            try
            {
                listenThread = new Thread(new ThreadStart(ServerObject.StartListen));
                listenThread.Start();
            }
            catch (Exception ex)
            {
                ServerObject.Shutdown();
                Console.WriteLine(ex.Message);
            }
        }
    }
}
