﻿using Query;
using Query.Client;
using Query.Server;
using ServerApp.Infrastructure;
using ServerApp.Controllers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;

namespace ServerApp.Objects
{
    public static class ServerObject
    {
        #region Private Members
        private static TcpListener TcpListener;
        private static BinaryFormatter Formatter = new BinaryFormatter();
        #endregion

        #region Public Members
        public static List<ServerRoom> Rooms { get; private set; } = new List<ServerRoom>();
        public static List<ClientObject> Users = new List<ClientObject>();
        #endregion

        #region Public Functions

        public static void RemoveConnection(ClientObject client)
        {
            Users.Remove(client);
        }

        public static void CreateConnection(ClientObject client)
        {
            Users.Add(client);
        }

        public static void Shutdown()
        {
            TcpListener.Stop();
            foreach (var user in Users)
                user.CloseConnection();
            Environment.Exit(0);
        }

        public static void RoomAction(ClientObject client, Room room)
        {
            object data = null;
            try
            {
                switch (room.Action)
                {
                    case Query.Client.Action.Create:
                        data = RoomCreator.Create(room.Data, false);
                        SendRoom(client, (Room)data);
                        MessageAction(client, new Message($"You created the new room with name {((Room)data).Data}", "System", ((Room)data).ID));
                        break;
                    case Query.Client.Action.Leave:
                        SendRoom(client, room);
                        break;
                    default:
                        Debugger.Break();
                        break;
                }
            }
            catch (Exception ex)
            {
                SendFail(client, ex.Message);
            }
        }

        public static void RoomAction(ClientObject sender, InviteToRoom data)
        {
            var inviteUser = Users.Select(x => x).
                Where(x => String.Compare(x.UserName, data.Invited, StringComparison.OrdinalIgnoreCase) == 0)
                .FirstOrDefault();
            if (inviteUser == null)
            {
                SendFail(sender, "Can not find this user");
                return;
            }
            var serverRoom = Rooms.Select(x => x)
                .Where(x => x.ID == data.Room.ID)
                .FirstOrDefault();
            if (serverRoom == null)
            {
                SendFail(sender, "Error during invite");
                return;
            }
            if (serverRoom.Users.Contains(inviteUser))
            {
                SendFail(sender, "This user has already been invited to the room");
                return;
            }
            SendRoom(inviteUser, data.Room);
            MessageAction(sender, new Message($"{inviteUser.UserName} was invited in room by {sender.UserName}", "System", data.Room.ID));
            SendNotification(sender, "User was successfully invited to the room =)");
        }

        public static void MessageAction(ClientObject sender, Message data)
        {
            if (data.Text.Length > Constant.MaxMessageLength)
            {
                SendFail(sender, "Message is too long");
                return;
            }
            var serverRoom = Rooms
                .Select(x => x)
                .Where(x => x.ID == data.RoomID)
                .FirstOrDefault();
            if (serverRoom == null)
            {
                SendFail(sender, "Error during message sending");
                return;
            }

            foreach (var user in serverRoom.Users.Where(x => x.IsAuth))
            {
                Formatter.Serialize(user.Stream, data);
            }
        }

        public static void SendAnswer(ClientObject receiver, bool success, string note)
        {
            Formatter.Serialize(receiver.Stream, new Answer { Success = success, Note = note });
        }

        public static void SendRoom(ClientObject receiver, Room room)
        {
            switch (room.Action)
            {
                case Query.Client.Action.Create:
                    receiver.Invite(room);
                    break;
                case Query.Client.Action.Leave:
                    receiver.LeaveRoom(room);
                    break;
            }
        }

        public static void SendNotification(ClientObject receiver, string text)
        {
            Formatter.Serialize(receiver.Stream, new Notification { Text = text });
        }

        public static void SendFail(ClientObject recevier, string text)
        {
            Formatter.Serialize(recevier.Stream, new Problem { Text = text });
        }

        public static void StartListen()
        {
            try
            {
                //62.109.7.50/
                TcpListener = new TcpListener(IPAddress.Any, 8080);
                TcpListener.Start();
                Console.WriteLine("Server was started. Waiting connections");
                while (true)
                {
                    var client = new ClientObject(TcpListener.AcceptTcpClient());
                    var clientThread = new Thread(new ThreadStart(client.StartProcess));
                    clientThread.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"StartListenEx: {ex.Message} \n {ex}");

                Shutdown();
            }
        }

        public static void RegisterRoom(ServerRoom newRoom)
        {
            if (newRoom != null)
                Rooms.Add(newRoom);
        }
        #endregion
    }
}
