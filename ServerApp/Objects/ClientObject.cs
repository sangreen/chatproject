﻿using Query;
using Query.Client;
using ServerApp.Infrastructure;
using ServerApp.Controllers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;

namespace ServerApp.Objects
{
    public class ClientObject
    {
        #region Public Members
        public NetworkStream Stream { get; private set; }
        public ServerRoom CurrentRoom { get; private set; }
        public List<ServerRoom> RoomList { get; private set; } = new List<ServerRoom>();
        public string UserName { get; private set; }
        public bool IsAuth { get; private set; } = false;
        #endregion

        #region Private Members
        private TcpClient Client;
        private const int BufferSize = 1024;
        private BinaryFormatter Formatter = new BinaryFormatter();
        #endregion

        #region Constructor
        public ClientObject(TcpClient client)
        {
            Client = client;
            ServerObject.CreateConnection(this);
            IsAuth = false;
        }
        #endregion

        #region Private Functions
        private void StartCommunication()
        {
            while (true)
            {
                try
                {
                    var data = Formatter.Deserialize(Stream);
                    ProcessData(data);

                }
                catch (Exception ex)
                {
                    ServerObject.SendFail(this, ex.Message);
                    break;
                }
            }
        }

        private void ProcessData(object data)
        {
            switch (data.GetType().Name)
            {
                case Constant.RoomClassName:
                    ServerObject.RoomAction(this, (Room)data);
                    break;
                case Constant.MessageClassName:
                    ServerObject.MessageAction(this, (Message)data);
                    break;
                case Constant.InviteClassName:
                    ServerObject.RoomAction(this, (InviteToRoom)data);
                    break;
            }
        }

        private void WaitAuthorization()
        {
            string userName = null;
            while (userName == null)
            {
                var authParams = (Auth)Formatter.Deserialize(Stream);
                if (authParams == null)
                    CloseConnection();
                var handler = Handler.CorrectNickParameters(authParams.Login);
                if (!handler.Item1)
                    ServerObject.SendAnswer(this, handler.Item1, handler.Item2);
                else
                    userName = authParams.Login;
            }
            UserName = userName;
            IsAuth = true;
            ServerObject.SendAnswer(this, true, "Welcome");
        }

        #endregion

        #region Public Functions

        public void CloseConnection()
        {
            IsAuth = false;
            foreach (var room in RoomList)
            {
                room.Remove(this);
                ServerObject.MessageAction(this, new Message($"{this.UserName} leave this room", "System", room.ID));
            }
            if (Client != null)
                Client.Close();
            if (Stream != null)
                Stream.Close();
        }

        public void LeaveRoom(Room room)
        {
            var leavingRoom = RoomList
                .Select(x => x)
                .Where(x => x.ID == room.ID)
                .FirstOrDefault();
            if (leavingRoom != null)
            {
                ServerObject.MessageAction(this, new Message($"{this.UserName} leave this room", "System", room.ID));
                leavingRoom.Remove(this);
                RoomList.Remove(leavingRoom);
                Formatter.Serialize(this.Stream, room);
            }
        }

        public void Invite(Room room)
        {
            var serverRoom = ServerObject
                .Rooms.Select(x => x)
                .Where(x => x.ID == room.ID)
                .FirstOrDefault();
            if (serverRoom == null)
            {
                ServerObject.SendFail(this, "Can not invite to room");
                Debugger.Break();
                return;
            }
            serverRoom.Invite(this);
            Formatter.Serialize(this.Stream, room);
        }

        public void StartProcess()
        {
            try
            {
                Console.WriteLine("A new user connection to server");
                Stream = Client.GetStream();
                if (!IsAuth)
                    WaitAuthorization();


                Console.WriteLine($"{UserName} was connected to server, start communication");
                StartCommunication();
            }
            catch (Exception ex)
            {
                var user = UserName == null ? UserName : string.Empty;
                Console.WriteLine($"{DateTime.Now.ToShortTimeString()} {ex.Message} {user}");
            }
            finally
            {
                ServerObject.RemoveConnection(this);
                CloseConnection();
            }
        }
        #endregion
    }
}
